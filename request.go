package gin_inter_microservice_comm

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type JsonResponse struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
}

func ContactService(httpMethod string, serviceURL string, requestBody any) (JsonResponse, error) {
	// marshal json body
	jsonData, err := json.Marshal(requestBody)
	if err != nil {
		return JsonResponse{}, errors.New("please provide a valid json body")
	}

	data := bytes.NewBuffer(jsonData)

	// create new http request
	request, _ := http.NewRequest(httpMethod, serviceURL, data)

	// create new http client
	client := http.DefaultClient

	// do the request and fetch for response
	response, err := client.Do(request)
	if err != nil {
		return JsonResponse{}, err
	}
	// close response body after finishing - not now
	defer response.Body.Close()

	// read received json data
	var responseFromService JsonResponse
	err = json.NewDecoder(response.Body).Decode(&responseFromService)
	if err != nil {
		return JsonResponse{}, err
	}

	return responseFromService, nil
}

func ErrorResponse(c *gin.Context, statusCode int, message string) {
	c.AbortWithStatusJSON(statusCode, JsonResponse{
		Error:   true,
		Message: message,
		Data:    nil,
	})
}
